﻿using System;
using System.Threading;
using SFML.Graphics;
using SFML.System;

namespace MultithreadedCars
{
    public class Train : IDisposable
    {
        private bool _abort;
        private readonly Game _game;

        public float Speed { get; } = 0.15f;
        public Sprite Sprite { get; }
        public bool Visible { get; private set; }

        public Train(Game game)
        {
            _game = game;

            var texture = new Texture("assets/train.png");
            Sprite = new Sprite(texture)
            {
                Origin = new Vector2f(32, 32)
            };

            var thread = new Thread(Update);
            thread.Start();
        }

        public void Update()
        {
            var clock = new Clock();

            while (!_abort)
            {
                Thread.Sleep(10);

                Move(clock.ElapsedTime.AsMilliseconds());

                clock.Restart();
            }
        }

        public void Start()
        {
            lock (_game.TrainLock)
            {
                Visible = true;
                Sprite.Position = new Vector2f(5, 70).ToAbsolute(_game.WindowSize);
            }
        }

        private void Move(int millis)
        {
            lock (_game.TrainLock)
            {
                if (Visible)
                {
                    Sprite.Position += new Vector2f(millis * Speed, 0);

                    if (Sprite.Position.ToRelative(_game.WindowSize).X >= 95)
                    {
                        Visible = false;
                    }
                }
            }
        }

        public void Dispose()
        {
            _abort = true;
            Sprite.Dispose();
        }
    }
}