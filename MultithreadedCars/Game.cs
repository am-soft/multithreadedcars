﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SFML.Graphics;
using SFML.System;

namespace MultithreadedCars
{
    public class Game : IDisposable
    {
        private readonly Barrier _barrier;
        private readonly List<Car> _cars = new List<Car>();
        private readonly Clock _clock = new Clock();
        private readonly int _numberOfCars;
        private readonly int _spawnTime;
        private readonly Train _train;
        public readonly object BarrierLock = new object();

        public readonly object CarsLock = new object();
        public readonly object TrainLock = new object();
        public readonly Vector2f WindowSize;
        private bool _abort;
        private long _lastCarSpawnedTimestamp;

        public Game(int numberOfCars, int spawnTime, Vector2f windowSize)
        {
            _numberOfCars = numberOfCars;
            _spawnTime = spawnTime;
            WindowSize = windowSize;

            _barrier = new Barrier(this);
            _train = new Train(this);

            var thread = new Thread(Update);
            thread.Start();
        }

        public void Dispose()
        {
            _abort = true;

            foreach (var car in _cars) car.Dispose();

            _barrier.Dispose();
            _train.Dispose();
        }

        public void Update()
        {
            while (!_abort)
            {
                Thread.Sleep(10);

                SpawnCars();
                DestroyCars();
                CheckCollisions();
                StartTrain();
            }
        }

        private void StartTrain()
        {
            lock (BarrierLock)
            lock (TrainLock)
            {
                if (_barrier.Locked && !_train.Visible)
                    _train.Start();
            }
        }

        private void SpawnCars()
        {
            lock (CarsLock)
            {
                if (_cars.Count != _numberOfCars)
                {
                    var now = _clock.ElapsedTime.AsMilliseconds();

                    if (_lastCarSpawnedTimestamp == 0 || now - _lastCarSpawnedTimestamp > _spawnTime)
                    {
                        _cars.Add(new Car(this));
                        _lastCarSpawnedTimestamp = now;
                    }
                }
            }
        }

        private void DestroyCars()
        {
            lock (CarsLock)
            {
                var finishedCars = _cars.Where(x => x.Finished).ToList();
                foreach (var car in finishedCars)
                {
                    car.Dispose();
                    _cars.Remove(car);
                }
            }
        }

        private void CheckCollisions()
        {
            lock (CarsLock)
            {
                foreach (var car in _cars)
                {
                    lock (BarrierLock)
                    {
                        var barrierDistance = Math.Sqrt(Math.Pow(_barrier.Position.X - car.Position.X, 2) +
                                                        Math.Pow(_barrier.Position.Y - car.Position.Y, 2));

                        if (barrierDistance < car.Size.X * 1.2 &&
                            car.Position.Y > _barrier.Position.Y + _barrier.Size.Y)
                        {
                            if (_barrier.Locked)
                                car.Halt();
                            else
                                car.Continue();
                        }
                    }

                    foreach (var otherCar in _cars)
                    {
                        if (otherCar == car)
                            continue;

                        var distance = Math.Sqrt(Math.Pow(otherCar.Position.X - car.Position.X, 2) +
                                                 Math.Pow(otherCar.Position.Y - car.Position.Y, 2));

                        if (distance < otherCar.Size.X * 1.2)
                            otherCar.SlowDown(car.Speed);
                    }
                }
            }
        }


        public void Draw(RenderWindow window)
        {
            lock (CarsLock)
            {
                foreach (var car in _cars) window.Draw(car.Sprite);
            }

            lock (BarrierLock)
            {
                if (_barrier.Locked) window.Draw(_barrier.Sprite);
            }

            lock (TrainLock)
            {
                if (_train.Visible) window.Draw(_train.Sprite);
            }
        }
    }
}