﻿using System;
using System.Threading;
using SFML.Graphics;
using SFML.System;

namespace MultithreadedCars
{
    public class Barrier : IDisposable
    {
        private Clock _clock = new Clock();
        private int _lastBarrierLockedTime;
        private int _nextBarrierLockTime;
        private const int BarrierLockTime = 4000;
        private bool _abort;
        private readonly Game _game;

        public Sprite Sprite { get; }
        public Vector2f Position => Sprite.Position;
        public bool Locked { get; set; }
        public Vector2f Size { get; } = new Vector2f(64, 64);

        public Barrier(Game game)
        {
            _game = game;

            var texture = new Texture("assets/barrier.png");
            Sprite = new Sprite(texture)
            {
                Position = new Vector2f(15.0f, 70.0f).ToAbsolute(game.WindowSize),
                Origin = new Vector2f(32, 32)
            };

            var random = new Random();
            _nextBarrierLockTime = random.Next(4000, 8000);

            var thread = new Thread(Update);
            thread.Start();
        }

        public void Update()
        {
            var random = new Random();

            while (!_abort)
            {
                Thread.Sleep(10);

                lock (_game.BarrierLock)
                {
                    var now = _clock.ElapsedTime.AsMilliseconds();

                    if (!Locked)
                    {
                        if (now >= _nextBarrierLockTime)
                        {
                            Locked = true;
                            _lastBarrierLockedTime = now;
                        }
                    }
                    else if (now - _lastBarrierLockedTime >= BarrierLockTime)
                    {
                        Locked = false;
                        _nextBarrierLockTime = now + random.Next(4000, 8000);
                    }
                }
            }
        }

        public void Dispose()
        {
            Sprite.Dispose();
            _abort = true;
        }
    }
}