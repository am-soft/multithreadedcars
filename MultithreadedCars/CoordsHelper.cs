﻿using SFML.System;

namespace MultithreadedCars
{
    public static class CoordsHelper
    {
        public static Vector2f ToRelative(this Vector2f vector, Vector2f windowSize)
        {
            vector.X = vector.X / windowSize.X * 100;
            vector.Y = vector.Y / windowSize.Y * 100;
            return vector;
        }

        public static Vector2f ToAbsolute(this Vector2f vector, Vector2f windowSize)
        {
            vector.X = windowSize.X * vector.X / 100;
            vector.Y = windowSize.Y * vector.Y / 100;
            return vector;
        }
    }
}