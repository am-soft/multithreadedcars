﻿using System;
using System.Threading;
using SFML.Graphics;
using SFML.System;

namespace MultithreadedCars
{
    public class Car : IDisposable
    {
        private readonly Game _game;
        private readonly Random _random = new Random();
        private bool _abort;

        public Car(Game game)
        {
            _game = game;

            var texture = new Texture("assets/car1.png");

            Sprite = new Sprite(texture)
            {
                Rotation = -90.0f,
                Position = new Vector2f(92, 82).ToAbsolute(_game.WindowSize),
                Origin = new Vector2f(32, 32)
            };

            Speed = _random.Next(4, 7) / 100.0f;

            var thread = new Thread(Update);
            thread.Start();
        }

        public Sprite Sprite { get; }
        public float Speed { get; set; }
        public Vector2f Position => Sprite.Position;
        public Vector2f Size => new Vector2f(64, 64);
        public bool Finished { get; private set; }

        public void Dispose()
        {
            _abort = true;
            Sprite?.Dispose();
        }

        public void Update()
        {
            var clock = new Clock();

            while (!_abort)
            {
                Thread.Sleep(10);

                Move(clock.ElapsedTime.AsMilliseconds());

                clock.Restart();
            }
        }

        public void Move(int millis)
        {
            var currentPosition = Sprite.Position.ToRelative(_game.WindowSize);

            if (currentPosition.X <= -4)
            {
                Finished = true;
                return;
            }

            if (currentPosition.X <= 79 && currentPosition.X > -5 && currentPosition.Y <= 32)
            {
                Sprite.Rotation = -90;
                Sprite.Position += new Vector2f(-millis * Speed, 0).ToAbsolute(_game.WindowSize);
                return;
            }

            if (currentPosition.X >= 78 && currentPosition.Y >= 32 && currentPosition.Y <= 59)
            {
                Sprite.Rotation = 0;
                Sprite.Position += new Vector2f(0, -millis * Speed).ToAbsolute(_game.WindowSize);
                return;
            }

            if (currentPosition.X >= 14 && currentPosition.X <= 78 && currentPosition.Y >= 57 &&
                currentPosition.Y <= 59)
            {
                Sprite.Rotation = 90;
                Sprite.Position += new Vector2f(millis * Speed, 0).ToAbsolute(_game.WindowSize);
                return;
            }

            if (Math.Abs(currentPosition.X - 15) < 1 && currentPosition.Y <= 82 && currentPosition.Y >= 58)
            {
                Sprite.Rotation = 0;
                Sprite.Position += new Vector2f(0, -millis * Speed).ToAbsolute(_game.WindowSize);
                return;
            }

            if (currentPosition.X <= 92.0f && currentPosition.X >= 15.0f && Math.Abs(currentPosition.Y - 82) < 1)
                Sprite.Position += new Vector2f(-millis * Speed, 0).ToAbsolute(_game.WindowSize);
        }

        public void Halt()
        {
            Speed = 0;
        }

        public void Continue()
        {
            Speed = _random.Next(4, 7) / 100.0f;
        }

        public void SlowDown(float newSpeed)
        {
            Speed = newSpeed;
        }
    }
}