﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace MultithreadedCars
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var close = false;

            var windowSize = new Vector2f(852, 640);
            var window = new RenderWindow(new VideoMode((uint) windowSize.X, (uint) windowSize.Y),
                "Multithreaded Cars");
            window.SetActive();

            window.Closed += (sender, eventArgs) => close = true;

            var backgroundTexture = new Texture("assets/map.png");
            var backgroundSprite = new Sprite(backgroundTexture);

            var game = new Game(6, 500, windowSize);

            while (window.IsOpen)
            {
                if (close)
                    break;

                window.Clear();
                window.DispatchEvents();

                window.Draw(backgroundSprite);
                game.Draw(window);

                window.Display();
            }

            game.Dispose();
        }
    }
}